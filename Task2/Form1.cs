﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace Task2
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent(); // Initialize all basic componnets of Form
        }

        //Passing Connection String to MySQLCOnnectio.
        MySqlConnection connection = new MySqlConnection("server=localhost;database=usermgt;port=3306;username=root;password=root2");

        //Creating Command before executing it
        MySqlCommand command;

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        //Load Form
        private void Form1_Load(object sender, EventArgs e)
        {
            //Calling Populate Data Grid View Method- fetch data from database and display it on main screen
            populateDGV();
        }

        //Popultate Data Grid View
        public void populateDGV()
        {
            // Select Query - populate the datagridview
            string selectQuery = "SELECT * FROM students";
            DataTable table = new DataTable();
            MySqlDataAdapter adapter = new MySqlDataAdapter(selectQuery, connection);

            //Fill the feteched data into data Grid
            adapter.Fill(table);
            dataGridView1.DataSource = table;
}

        //Add Button- Add Record Method
        private void addButton_Click(object sender, EventArgs e)
        {
            //Insert Data 
            string insertQuery = "INSERT INTO students(name,roll,class,section) VALUES('" + nameBox.Text + "','" + rollBox.Text + "','" + classBox.Text + "','" + sectionBox.Text + "')";
            executeMyQuery(insertQuery); //Pass query to execute Method
            populateDGV(); // Calling Populate Method
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            


        }

        //Open Connection
        public void openConnection()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
        }

        //Close Connection
        public void closeConnection()
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }

        //Helper method to execute Queries
        public void executeMyQuery(string query)
        {
            try
            {
                //Open connection
                openConnection();
                command = new MySqlCommand(query, connection); //Pass Query to Connection

                if (command.ExecuteNonQuery() == 1)
                {
                    // MessageBox.Show("Query Executed");
                }

                else
                {
                    // MessageBox.Show("Query Not Executed");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); //Exception message incase something goes wrong
            }
            finally
            {
                closeConnection(); // close connection
            }
        }

        //Update Button - Update Record Method
        private void updateButton_Click(object sender, EventArgs e)
        {
            //Update Record only if a Row is currently Selected
            if (idBox.Text != "")
            {
                string updateQuery = "UPDATE students SET name='" + nameBox.Text + "',roll='" + rollBox.Text + "',class='" + classBox.Text + "',section='" + sectionBox.Text + "' WHERE id =" + Convert.ToInt32(idBox.Text);
                executeMyQuery(updateQuery);
                populateDGV();
            }

            //Do not update record if no row is selected
            else
            {
                MessageBox.Show("Please select a data to udpate");
            }

            //clear form fields after updating record
            nameBox.Clear();
            idBox.Clear();
            rollBox.Clear();
            sectionBox.Clear();
            classBox.Clear();
        }

        //Delete Button - Delete Record Method
        private void deleteButton_Click(object sender, EventArgs e)
        {

            //Delete record if a row is selected
            if (idBox.Text!="")
            {
                string deleteQuery = "DELETE FROM students WHERE id = " + int.Parse(idBox.Text);
                executeMyQuery(deleteQuery);
                populateDGV();
            }

            //Do not delete record if no row is selected 
            else
            {
                MessageBox.Show("no data selected");
            } 

            
            //clear form fields after deleting record
            nameBox.Clear();
            idBox.Clear();
            rollBox.Clear();
            sectionBox.Clear();
            classBox.Clear();

        }

        private void searchButton_Click(object sender, EventArgs e)
        {
        }

        private void nameBox_MouseClick(object sender, MouseEventArgs e)
        {

        }

        //Data Grid View
        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            idBox.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            nameBox.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            rollBox.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            classBox.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            sectionBox.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();

        }

        //Clear form fields 
        private void clearButton_Click(object sender, EventArgs e)
        {
            idBox.Clear();
            nameBox.Clear();
            rollBox.Clear();
            classBox.Clear();
            sectionBox.Clear();
        }

        private void nameBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
